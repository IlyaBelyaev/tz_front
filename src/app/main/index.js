import React, { Component } from 'react';
import Header from '../components/header/header';
import Body from './body/body';

class Home extends Component {
    render() {
        return (
            <div className="off-canvas-menu">
                <Header/>
                <Body/>
            </div>
        )
    }
}

export default Home;