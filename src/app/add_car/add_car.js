import React, {Component} from 'react';
import Header from '../components/header/header';
import jwt from 'jsonwebtoken';
import profile_img from '../../img/png-36.png';
import { Link } from 'react-router-dom';

class Sell_car extends Component {
    constructor(props) {
        super(props);

        this.name_ref = React.createRef();
        this.img_ref = React.createRef();
        this.img_input_ref = React.createRef();
    }

    render() {
        return (
            <div className="off-canvas-menu">
                <Header/>
                <div className="wrapper" style={{marginTop: '100px'}}>
                    <div className="main">
                        <div className="section">
                            <div className="container">
                                <h3>Добавить машину</h3>
                                <form>
                                    <div className="row">
                                        <div className="col-md-5 col-sm-5">
                                            <h6>Изображение машины</h6>
                                            <div className="fileinput fileinput-new text-center" data-provides="fileinput">
                                                <div className="fileinput-new thumbnail img-no-padding" style={{maxWidth: '370px', maxHeight: '250px'}}>
                                                    <img ref={this.img_ref} src={profile_img} alt="..."/>
                                                </div>
                                            </div>
                                            <div className="custom-file">
                                                <input ref={this.img_input_ref} onChange={this.changeImg.bind(this)} type="file" className="custom-file-input" id="customFile"/>
                                                    <label className="custom-file-label" for="customFile">Выберите файл</label>
                                            </div>
                                        </div>

                                        <div className="col-md-7 col-sm-7">
                                            <div className="form-group">
                                                <h6>Название</h6>
                                                <input ref={this.name_ref} type="text" className="form-control border-input"/>
                                            </div>
                                        </div>
                                    </div>

                                    <br/>
                                    <br/>

                                    <div className="row buttons-row">
                                        <div className="col-md-6 col-sm-6">
                                            <Link to='/personal_area' style={{color: 'black'}} className="btn btn-outline-danger btn-block btn-round">Назад</Link>
                                        </div>
                                        <div className="col-md-6 col-sm-6">
                                            <Link to='/personal_area' onClick={this.add.bind(this)} style={{color: 'white'}} className="btn btn-primary btn-block btn-round">Добавить</Link>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    add(){
        let formData = new FormData();

        const id_user = jwt.decode(getCookie('token'))._id;

        formData.append('name', this.name_ref.current.value);
        formData.append('id_user', id_user);
        formData.append('image', this.img_input_ref.current.files[0]);

        //const headers = new Headers();
        //headers.append('Content-Type', 'multipart/form-data');

        fetch('http://localhost:5000/add_car', {
            method: 'POST',
            credentials: 'include',
            //headers: headers,
            body: formData
        });


        function getCookie(name) {
            const matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }
    }

    changeImg(){
        try {
            const reader = new FileReader();

            reader.onload = function (e) {
                this.img_ref.current.setAttribute('src', e.target.result);
            }.bind(this);

            reader.readAsDataURL(this.img_input_ref.current.files[0]);
        }catch (err){}
    }
}

export default Sell_car;